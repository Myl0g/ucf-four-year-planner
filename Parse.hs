module Parse
(
    parseCourseDivision
,   parseCourseNumber
,   parseCourse
) where

import Data.Maybe ( isNothing, fromJust )
import Text.Read ( readMaybe )
import Structures ( Course(..) )

parseCourseDivision :: String -> String
parseCourseDivision = take 3

parseCourseNumber :: String -> Int 
parseCourseNumber input
    | isNothing result = error "course number not provided"
    | otherwise = fromJust result
    where result = readMaybe (drop 3 input) :: Maybe Int

parseCourse :: String -> [Structures.Course] -> [Structures.Course] -> Structures.Course
parseCourse str prereqs coreqs = Structures.Course { division = parseCourseDivision str, number = parseCourseNumber str, prerequisites = prereqs, corequisites = coreqs }
