module Structures
( Course (..)
, Program (..)
) where

data Course = Course
    {
        division :: String
    ,   number :: Int
    ,   prerequisites :: [Course]
    ,   corequisites :: [Course]
    -- ,   semesters :: [String]
    } deriving (Eq, Show, Read)

data Program = Program
    {
        name :: String
    ,   courses :: [Course]
    }
